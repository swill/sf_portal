package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/spf13/viper"
)

var (
	tpl              map[string]*template.Template
	cm               map[string]*CookieMap
	use_local_static = false
	base_url         = "https://apps.uhhg.org/Citrix/DefaultWeb/"
)

type CookieMap struct {
	Name     string
	Path     string
	Secure   bool
	HttpOnly bool
}

type ResourcesList struct {
	Unauthorized           bool `json:"unauthorized"`
	IsSubscriptionEnabled  bool `json:"isSubscriptionEnabled"`
	IsUnauthenticatedStore bool `json:"isUnauthenticatedStore"`
	Resources              []struct {
		ClientTypes           []string `json:"clienttypes"`
		IconUrl               string   `json:"iconurl"`
		ID                    string   `json:"id"`
		Keywords              []string `json:"keywords"`
		LaunchStatusUrl       string   `json:"launchstatusurl"`
		LaunchUrl             string   `json:"launchurl"`
		Mandatory             bool     `json:"mandatory"`
		Name                  string   `json:"name"`
		Path                  string   `json:"path"`
		Position              string   `json:"position"`
		ShortcutValidationUrl string   `json:"shortcutvalidationurl"`
		SubscriptionStatus    string   `json:"subscriptionstatus"`
		Content               string   `json:"content,omitempty"`
	} `json:"resources"`
	Name string
}

type AuthMethods struct {
	Xmlns  string `xml:"xmlns,attr"`
	Method []struct {
		Name string `xml:"name,attr"`
		Url  string `xml:"url,attr"`
	} `xml:"method"`
}

type AuthenticateResponse struct {
	Xmlns                      string `xml:"xmlns,attr"`
	Status                     string
	Result                     string
	AuthenticationRequirements struct {
		PostBack         string
		CancelPostBack   string
		CancelButtonText string
		Requirements     []struct {
			Requirement struct {
				Credential struct {
					ID     string
					SaveID string
					Type   string
				}
				Label struct {
					Text string
					Type string
				}
				Input struct {
					AssistiveText string
					Text          struct {
						Secret     bool
						ReadOnly   bool
						Constraint string
					}
					CheckBox struct {
						InitialValue bool
					}
					Button string
				}
			}
		}
	}
}

func main() {
	tpl = make(map[string]*template.Template)
	cm = make(map[string]*CookieMap)

	// read config from a file
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Error parsing config file: %s", err)
	}

	// set global defaults
	viper.SetDefault("port", 8080)

	// validate we have all the config required to start
	missing_config := false
	if !viper.IsSet("names") {
		log.Printf("Error: Missing required 'names' in the config file.\n")
		missing_config = true
	}
	if !viper.IsSet("urls") {
		log.Printf("Error: Missing required 'urls' in the config file.\n")
		missing_config = true
	}
	if viper.IsSet("names") && viper.IsSet("urls") && len(viper.GetStringSlice("names")) != len(viper.GetStringSlice("urls")) {
		log.Printf("Error: The length of 'names' and 'urls' must be equal in the config file.\n")
		missing_config = true
	}
	if missing_config {
		log.Fatal("Missing required configuration details, please update the config file.")
	}

	// setup template functions
	func_map := template.FuncMap{
		"raw": func(msg interface{}) template.HTML { return template.HTML(msg.(template.HTML)) },
	}
	// setup templates
	tpl["index"] = template.Must(template.New("").Funcs(func_map).Parse(fmt.Sprintf("%s%s",
		FSMustString(use_local_static, "/static/views/index.html"),
		FSMustString(use_local_static, "/static/views/layout.html"))))

	// handle the url routing
	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/proxy/", handleProxy)
	http.Handle("/static/", http.FileServer(FS(use_local_static)))

	// serve single root level files
	handleSingle("/favicon.ico", "/static/img/favicon.png")

	// start the web server
	log.Printf("storefront portal started - http://localhost:%d\n", viper.GetInt("port"))
	http.ListenAndServe(fmt.Sprintf(":%d", viper.GetInt("port")), nil)
}

type PageIndex struct {
	Title          string
	ResourcesLists []*ResourcesList
}

// index page (dashboard)
func handleIndex(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		handleError(w, r, 404, "The page you are looking for does not exist.")
		return
	}

	page := &PageIndex{
		Title: "StoreFront Portal",
	}

	base_urls := viper.GetStringSlice("urls")
	src_cookies := r.Cookies()
	var cl []*http.Cookie

	is_logged_in := make([]bool, len(base_urls))
	for i := range is_logged_in {
		is_logged_in[i] = false
	}

	resource_list := make([]*ResourcesList, len(base_urls))
	for i := range resource_list {
		resource_list[i] = nil
	}

	for i, base := range base_urls {
		var res_list, res_auth, res_login *http.Response
		var err error
		_, cl, err = sfRequest(i, src_cookies, base, "Home/Configuration", "POST", "application/xml", nil)
		if err != nil {
			log.Println("StoreFront Error for 'Home/Configuration':", err.Error())
		} else {
			for _, cookie := range cl {
				http.SetCookie(w, cookie)
			}

			res_list, cl, err = sfRequest(i, cl, base, "Resources/List", "POST", "application/json", nil)
			if err != nil {
				log.Println("StoreFront Error for 'Resources/List':", err.Error())
			} else {
				decoder := json.NewDecoder(res_list.Body)
				var resources ResourcesList
				err := decoder.Decode(&resources)
				if err != nil {
					log.Println("StoreFront 'Resources/List' json error:", err.Error())
				} else {
					if resources.Unauthorized {
						// try to get the login details for this connection
						res_auth, cl, err = sfRequest(i, cl, base, "Authentication/GetAuthMethods", "POST", "application/xml", nil)
						if err != nil {
							log.Println("StoreFront Error for 'Authentication/GetAuthMethods':", err.Error())
						} else {
							auth_decoder := xml.NewDecoder(res_auth.Body)
							var auth_methods AuthMethods
							err := auth_decoder.Decode(&auth_methods)
							if err != nil {
								log.Println("StoreFront 'Authentication/GetAuthMethods' xml error:", err.Error())
							}
							defer res_auth.Body.Close()

							//body_login, err := ioutil.ReadAll(res_auth.Body)
							//if err != nil {
							//	log.Printf("Body Error: ", err)
							//}
							//log.Printf("Body:\n%#v", string(body_login))

							// Find an auth method we know how to handle
							for _, method := range auth_methods.Method {
								log.Printf("%+v\n", method)
								lower_method := strings.ToLower(method.Name)
								if lower_method == "integratedwindows" {
									res_login, cl, err = sfRequest(i, cl, base, method.Url, "POST", "", nil)
									if err != nil {
										log.Println("StoreFront Error for 'DomainPassthroughAuth/Login':", err.Error())
									} else {
										//login_decoder := xml.NewDecoder(res_login.Body)
										//var auth_resonse AuthenticateResponse
										//err := login_decoder.Decode(&auth_resonse)
										//if err != nil {
										//	log.Println("StoreFront 'ExplicitAuth/Login' xml error:", err.Error())
										//}
										//defer res_login.Body.Close()
										//log.Printf("%+v\n", auth_resonse)

										log.Printf("Src Headers:\n%#+v\n", r.Header)
										log.Printf("Res Headers:\n%#+v\n", res_login.Header)
										body_login, err := ioutil.ReadAll(res_login.Body)
										if err != nil {
											log.Printf("Body Error: ", err)
										}
										log.Printf("Body:\n%s\n", string(body_login))
									}
								}
								//switch strings.ToLower(method.Name) {
								//case "explicitforms":
								//	res_login, cl, err = sfRequest(i, cl, base, "ExplicitAuth/Login", "POST", "application/xml", nil)
								//	if err != nil {
								//		log.Println("StoreFront Error for 'ExplicitAuth/Login':", err.Error())
								//	} else {
								//		login_decoder := xml.NewDecoder(res_login.Body)
								//		var auth_resonse AuthenticateResponse
								//		err := login_decoder.Decode(&auth_resonse)
								//		if err != nil {
								//			log.Println("StoreFront 'ExplicitAuth/Login' xml error:", err.Error())
								//		}
								//		defer res_login.Body.Close()
								//		log.Printf("%+v\n", auth_resonse)
								//
								//		//body_login, err := ioutil.ReadAll(res_login.Body)
								//		//if err != nil {
								//		//	log.Printf("Body Error: ", err)
								//		//}
								//		//log.Printf("Body:\n%#v", string(body_login))
								//	}
								//}
							}
						}
					} else {
						//log.Printf("%+v/n", resources)
						is_logged_in[i] = true
						resource_list[i] = &resources
					}
				}
				defer res_list.Body.Close()
			}
		}
	}

	for l, list := range resource_list {
		if list != nil {
			list.Name = viper.GetStringSlice("names")[l]
			for r, resource := range list.Resources {
				resource_list[l].Resources[r].IconUrl = fmt.Sprintf("/proxy/%d/%s",
					l, strings.TrimLeft(resource.IconUrl, "/"))
				resource_list[l].Resources[r].LaunchUrl = fmt.Sprintf("/proxy/%d/%s",
					l, strings.TrimLeft(resource.LaunchUrl, "/"))
				resource_list[l].Resources[r].LaunchStatusUrl = fmt.Sprintf("/proxy/%d/%s",
					l, strings.TrimLeft(resource.LaunchStatusUrl, "/"))
				resource_list[l].Resources[r].ShortcutValidationUrl = fmt.Sprintf("/proxy/%d/%s",
					l, strings.TrimLeft(resource.ShortcutValidationUrl, "/"))
			}
			page.ResourcesLists = append(page.ResourcesLists, list)
		}
	}

	// render the page...
	if err := tpl["index"].ExecuteTemplate(w, "layout", page); err != nil {
		log.Printf("Error executing template: %s\n", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	return
}

func handleProxy(w http.ResponseWriter, r *http.Request) {
	headers := []string{
		"X-Citrix-Application",
		"Content-Length",
		"Content-Type",
	}
	path := r.URL.Path
	for i, base_url := range viper.GetStringSlice("urls") {
		seg_len := 8 + len(strconv.FormatInt(int64(i), 10))
		if path[0:seg_len] == fmt.Sprintf("/proxy/%d/", i) {
			path = path[seg_len:]
			if len(r.URL.RawQuery) > 0 {
				path = fmt.Sprintf("%s?%s", path, r.URL.RawQuery)
			}
			res, _, err := sfRequest(i, r.Cookies(), base_url, path, "GET", "", nil)
			if err != nil {
				log.Println("StoreFront Error:", path, err.Error())
				http.Error(w, "Not found in StoreFront", 404)
			} else {
				defer res.Body.Close()
				// copy headers before sending the body
				for k := range res.Header {
					if in_strings(k, headers) {
						w.Header().Set(k, res.Header.Get(k))
					}
				}
				_, err := io.Copy(w, res.Body)
				if err != nil {
					log.Println("Body copy failed: ", path, err.Error())
					http.Error(w, "Failed to proxy content", 500)
				}
			}
		}
	}
}

func sfRequest(index int, src_cookies []*http.Cookie, base_url, uri, method, accepts string, body io.Reader) (*http.Response, []*http.Cookie, error) {
	cookie_list := make([]*http.Cookie, 0)
	url := fmt.Sprintf("%s/%s",
		strings.TrimRight(strings.TrimSpace(base_url), "/"),
		strings.TrimLeft(strings.TrimSpace(uri), "/"))
	client := &http.Client{}
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		log.Printf("Error in NewRequest: ", err)
		return nil, nil, err
	}

	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	if accepts != "" {
		req.Header.Set("Accept", accepts)
	}
	if strings.ToLower(method) == "post" {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	is_https := "No"
	if strings.ToLower(url)[0:5] == "https" {
		is_https = "Yes"
	}
	req.Header.Set("X-Citrix-IsUsingHTTPS", is_https)

	if strings.ToLower(uri) == "domainpassthroughauth/login" {
		req.Header.Set("User-Agent",
			"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)")
		req.Header.Set("Accept",
			"application/x-ms-application, image/jpeg, application/xaml+xml, image/gif, image/pjpeg, application/x-ms-xbap, */*")
	}

	// update cookie mapping
	for name, saved := range cm {
		if strings.HasPrefix(name, fmt.Sprintf("%d_", index)) {
			for _, src_cookie := range src_cookies {
				if src_cookie.Name == name {
					cookie := src_cookie
					cookie.Name = saved.Name
					cookie.Path = saved.Path
					cookie.Secure = saved.Secure
					cookie.HttpOnly = saved.HttpOnly
					req.AddCookie(cookie)

					if strings.ToLower(cookie.Name) == "csrftoken" {
						req.Header.Set("Csrf-Token", cookie.Value)
					}
				}
			}
		}
	}

	if strings.ToLower(uri) == "domainpassthroughauth/login" {
		log.Printf("Req Headers:\n%#+v\n", req.Header)
	}

	res, err := client.Do(req)
	if err != nil {
		log.Printf("Error in Do: ", err)
		return nil, nil, err
	}

	for _, cookie := range res.Cookies() {
		unique_name := fmt.Sprintf("%d_%s", index, cookie.Name)
		cm[unique_name] = &CookieMap{
			Name:     cookie.Name,
			Path:     cookie.Path,
			Secure:   cookie.Secure,
			HttpOnly: cookie.HttpOnly,
		}
		cookie.Name = unique_name
		cookie.Path = "/"
		cookie.Secure = false
		cookie.HttpOnly = true
		cookie_list = append(cookie_list, cookie)
	}

	return res, cookie_list, nil
}

// match a url pattern and serve a single file in response.
// eg: /favicon.ico from /static/img/favicon.ico
func handleSingle(pattern string, filename string) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		b, err := FSByte(use_local_static, filename)
		if err != nil {
			log.Printf("Error serving single file: %s\n %s\n", filename, err.Error())
		}
		w.Write(b)
	})
}

type PageError struct {
	Title     string
	ErrorCode int
	ErrorDesc string
}

// handle any http error you encounter
func handleError(w http.ResponseWriter, r *http.Request, status int, desc string) {
	title := http.StatusText(status)
	if title == "" {
		title = "Unknown Error"
	}
	page := &PageError{
		Title:     title,
		ErrorCode: status,
		ErrorDesc: desc,
	}
	w.WriteHeader(page.ErrorCode)
	if err := tpl["error"].ExecuteTemplate(w, "layout", page); err != nil {
		http.Error(w, page.ErrorDesc, page.ErrorCode)
	}
}

func in_strings(query string, strings []string) bool {
	for _, x := range strings {
		if x == query {
			return true
		}
	}
	return false
}
